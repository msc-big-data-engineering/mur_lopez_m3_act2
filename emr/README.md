# Creación de un cluster de EMR

Utilizaremos el código creado en el módulo 2 que realizar la clasficación por liga y temporada.

Lo primero será crear el bucket en Amazon S3 para poder acceder a los datos.

```
aws s3api create-bucket \
    --bucket mur-lopez-m3-act2
```

Habilitamos el versionado y restringimos el acceso público
```
aws s3api put-bucket-versioning \
    --bucket mur-lopez-m3-act2 \
    --versioning-configuration Status=Enabled

aws s3api put-public-access-block \
    --bucket mur-lopez-m3-act2 \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
```

Despúes sincronizamos el directorio con los datos que se encuentran en local con los del bucket de S3
```bash
aws s3 sync s3/league-classification/ s3://mur-lopez-m3-act2/league-classification
```

A continuación, hay que compilar el proyecto si no disponemos del ejecutable y para eso necesitamos tener instalado `sbt`. Para compilar el proyecto, nos situamos dentro del directorio `spark-league-classification` y ejecutamos `sbt package`. Una vez finalizada la compilación copiamos el ejecutable en el bucket creado anteriormente:
```
aws s3 cp spark-league-classification/target/scala-2.11/spark-league-classification_2.11-1.0.0.jar s3://mur-lopez-m3-act2
```

Para crear el cluster de AWS EMR, primero creamos los roles por defecto:
```
aws emr create-default-roles
```

Después eliminamos el directorio de salida y logs en caso de que existieran debido a ejecuciones anteriores:
```
aws s3 rm s3://mur-lopez-m3-act2/league-classification/out 
aws s3 rm s3://mur-lopez-m3-act2/logs 
```

Creamos un vpc por defecto en caso de no disponer de uno:
````
aws ec2 create-default-vpc
```

A continuación listamos las subredes disponibles:
```
aws ec2 describe-subnets
```

Elegimos la que aperece o en su defecto la que nos interese y en el parámetro `--ec2-attributes` sustituimos el atributo `SubnetId=` por el que corresponda.

En el archivo steps.json está la rutina que se debe ejecura en este clúster, en este caso es un `CUSTOM_JAR` que ejecuta un spark-submit con los datos necesarios para realizar la operación.

Para crear el clúster bastará con ejecutar el siguiente comando:
```
aws emr create-cluster \
     --name mbde_mur_lopez_m3_act2 \
     --release-label emr-5.9.0 \
     --instance-type m4.large --instance-count 3 \
     --applications Name=Spark \
     --steps file://steps.json \
     --log-uri 's3n://mur-lopez-m3-act2/logs' \
     --use-default-roles \
     --ec2-attributes SubnetId=subnet-0c7efce5ba0c2cf9d \
     --auto-terminate
```

Para recuperar los datos podemos sincronizar dichos directorios y obtener la salida en el directorio s3/league-classification/out
```
aws s3 sync s3://mur-lopez-m3-act2/league-classification s3/league-classification/
```
