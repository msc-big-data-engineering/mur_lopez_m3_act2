aws s3api create-bucket \
    --bucket mur-lopez-m3-act2

aws s3api put-bucket-versioning \
    --bucket mur-lopez-m3-act2 \
    --versioning-configuration Status=Enabled

aws s3api put-public-access-block \
    --bucket mur-lopez-m3-act2 \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

aws s3 sync s3/league-classification/ s3://mur-lopez-m3-act2/league-classification

aws s3 cp spark-league-classification/target/scala-2.11/spark-league-classification_2.11-1.0.0.jar s3://mur-lopez-m3-act2

aws emr create-default-roles

aws s3 rm s3://mur-lopez-m3-act2/league-classification/out 
aws s3 rm s3://mur-lopez-m3-act2/logs 

aws ec2 create-default-vpc

aws ec2 describe-subnets

aws emr create-cluster \
     --name mbde_mur_lopez_m3_act2 \
     --release-label emr-5.9.0 \
     --instance-type m4.large --instance-count 3 \
     --applications Name=Spark \
     --steps file://steps.json \
     --log-uri 's3n://mur-lopez-m3-act2/logs' \
     --use-default-roles \
     --ec2-attributes SubnetId=subnet-0c7efce5ba0c2cf9d \
     --auto-terminate

aws s3 sync s3://mur-lopez-m3-act2/league-classification s3/league-classification/

