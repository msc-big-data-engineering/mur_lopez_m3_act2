name := """spark-league-classification"""

version := "1.0.0"

scalaVersion := "2.11.12"


libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-streaming" % "2.3.2"  % "provided",
)
