package edu.ub.il3.mbde

import org.apache.spark.{SparkConf, SparkContext}

object LeagueClassification {

  def main(args: Array[String]) {

    if (args.length == 0) {
      println("You have to provide soccer files basepath")
      System.exit(1)
    }

    val soccerFilesBasePath = args(0)

    val sparkConfiguration = new SparkConf()
      .setAppName("LeagueClassification")

    // Let's create the Spark Context using the configuration we just created
    val sparkContext = new SparkContext(sparkConfiguration)

    // Lectura de archivo
    val rddMatchesFromFile =
      sparkContext.textFile(soccerFilesBasePath + "/league-classification/in")

    // Obtención de las columnas relevantes
    val rddMatches = rddMatchesFromFile
      .mapPartitionsWithIndex { (idx, iter) =>
        if (idx == 0) iter.drop(1) else iter
      }
      .map(l => {
        val r = l.split(",")
        (r(0).toInt, r(1), r(2).toInt, r(3).toInt, r(4).toInt, r(5).toInt)
      })

    // Generación de un RDD con los puntos de cada partido y equipo
    val teamMatchPoints = rddMatches.flatMap {
      case (lid, sid, hid, aid, hg, ag) => {
        if (hg > ag) {
          List(((lid, sid, hid), 3))
        } else if (ag < hg) {
          List(((lid, sid, aid), 3))
        } else {
          List(((lid, sid, hid), 1), ((lid, sid, aid), 1))
        }
      }
    }

    // Sumatorio de los puntos por liga, temporada y equipo

    val teamPoints = teamMatchPoints.reduceByKey(_ + _)

    // Clase para realizar una ordenación personalizada
    case class LeagueSeasonPoints(league: Int, season: String, points: Int)

    //Sobreescritura del metodo de ordenación para la clasificación
    implicit val sortLeagueBySeasonPoints = new Ordering[LeagueSeasonPoints] {
      override def compare(x: LeagueSeasonPoints, y: LeagueSeasonPoints) = {
        if (x.league.compareTo(y.league) != 0)
          x.league.compareTo(y.league)
        else if (x.season.compareTo(y.season) != 0)
          x.season.compareTo(y.season)
        else
          -x.points.compareTo(y.points)
      }
    }

    // Ordenación de los resultados según liga, temporada y puntos
    val leaguePointsSorted = teamPoints
      .map {
        case (k, v) => {
          (LeagueSeasonPoints(k._1, k._2, v), k._3)
        }
      }
      .sortByKey(true)

    // Reagrupación por grupos liga-temporada para realizar clasificación
    val leagueSorted = leaguePointsSorted.map {
      case (k, v) => {
        ((k.league, k.season), (v, k.points))
      }
    }

    // Mediante agrupación por clave, flatMap y zipWithIndex se puede realizar la clasificación.
    // La agrupación por clave construye un vector de valores por clave. Es decir,
    // el valor de cada clave es un vector con los valores que comparten clave, no se pierde el orden.
    // Mediate zipWithIndex aplicado a los valores del registro se crea un mapa (valor, indice) dónde
    // indice será la clasificación. Finalmente descomprmimos la información para el
    // flatMap
    val leagueClasification = leagueSorted
      .groupByKey()
      .flatMap(g =>
        g._2.zipWithIndex.map {
          case (v, index) => (g._1._1, g._1._2, v._1, v._2, index + 1)
        }
      )

    // Obtención de los equipos para el join
    val rddTeamsFromFile = sparkContext.textFile(
      soccerFilesBasePath + "/league-classification/soccer/Team.csv"
    )
    // Obtención de las columnas relevantes
    val rddTeams = rddTeamsFromFile
      .mapPartitionsWithIndex { (idx, iter) =>
        if (idx == 0) iter.drop(1) else iter
      }
      .map(l => {
        val r = l.split(",")
        (r(1).toInt, r(3))
      })

    // Obtención de las ligas para el join
    val rddLeagueFromFile = sparkContext.textFile(
      soccerFilesBasePath + "/league-classification/soccer/League.csv"
    )

    val rddLeagues = rddLeagueFromFile
      .mapPartitionsWithIndex { (idx, iter) =>
        if (idx == 0) iter.drop(1) else iter
      }
      .map(l => {
        val r = l.split(",")
        (r(0).toInt, r(2))
      })

    // Join por clave realizando mapeos sucesivos para realizar los join oportunos,
    // finalmente, ordenación por liga, temporada y clasificación
    val result = leagueClasification
      .map {
        case (lid, sid, tid, points, rank) =>
          (lid, (sid, tid, points, rank))
      }
      .join(rddLeagues)
      .map {
        case (lid, ((sid, tid, points, rank), lname)) =>
          (tid, (lname, sid, points, rank))
      }
      .join(rddTeams)
      .map {
        case (tid, ((lname, sid, points, rank), tname)) =>
          (lname, sid, tname, points, rank)
      }
      .sortBy(r => (r._1, r._2, r._4), ascending = false)

    // Guardado de los resultados en formato CSV
    result
      .map {
        case (lname, sid, tname, points, rank) =>
          lname.toString + "," + sid.toString + "," + tname.toString + "," + points.toString + "," + rank.toString
      }
      .saveAsTextFile(soccerFilesBasePath + "/league-classification/out")

    sparkContext.stop()
  }
}
