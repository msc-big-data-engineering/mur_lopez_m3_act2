# Instalación de Wordpress en AWS

## Instalación mediante aws cli

Lo primero es importar un par de claves ssh indicando la clave pública:
```bash
aws ec2 import-key-pair --key-name "mur_lopez_m3_act2" --public-key-material file://RUTA_A_CLAVE_PUBLICA.pub
```

Después hay que crear grupos de seguridad para permitir el acceso a los servicios HTTP y SSH.
Creamos un grupo con el nombre 'webserver' con permiso de ingreso en los puertos 80 y 443, y un grupo 'ssh' con permiso de ingreso en el puerto 22. Ambos grupos con permiso de salida de tráfico.

```bash
aws ec2 create-security-group --group-name webserver --description "webserver"

aws ec2 authorize-security-group-ingress --group-name webserver --protocol tcp --port 80 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name webserver --protocol tcp --port 443 --cidr 0.0.0.0/0
#aws ec2 authorize-security-group-egress --group-name webserver --protocol -1 --port 0 --cidr 0.0.0.0/0

aws ec2 create-security-group --group-name ssh --description "ssh"
aws ec2 authorize-security-group-ingress --group-name ssh --protocol tcp --port 22 --cidr 0.0.0.0/0
```
Con las siguientes líneas obtenemos los identificadores de los groupos de seguridad creados anteriormente para su posterior uso:
```bash
AWS_DEFAULT_SG=`aws ec2 describe-security-groups --group-names "default" --output text --query "SecurityGroups[*].GroupId"`
AWS_WS_SG=`aws ec2 describe-security-groups --group-names "webserver" --output text --query "SecurityGroups[*].GroupId"`
AWS_SSH_SG=`aws ec2 describe-security-groups --group-names "ssh" --output text --query "SecurityGroups[*].GroupId"`
```

A continuación crearemos en el servicio RDS una base de datos con el motor MariaDB y los datos de acceso que queramos:
```bash
aws rds create-db-instance \
    --allocated-storage 20 --db-instance-class db.t2.micro \
    --db-instance-identifier mur-lopez-m3-act2-wordpress \
    --engine mariadb \
    --enable-cloudwatch-logs-exports '["audit","error","general","slowquery"]' \
    --db-name wordpress \
    --master-username wordpress \
    --master-user-password 'W0rdpress!'
```

Esperaremos hasta que la instancia este creada e incializada y podamos obtener su dirección de acceso. 
```bash
echo "Database access: RDS_ENDPOINT="$(aws rds describe-db-instances --db-instance-identifier mur-lopez-m3-act2-wordpress --output json --query "DBInstances[0].Endpoint.Address")
```

Por últmo queda crear la instance EC2 que alojará el acceso a Wordpress. Creamos una máquina con la imagen de Ubuntu Server localizada desde la propia plataforma (id=ami-046842448f9e74e7d), le indicamos el tipo de instance, el nombre del par de claves creado anteriormente y los grupos de seguridad definidos también anteriormente.

```bash
aws ec2 run-instances --image-id "ami-046842448f9e74e7d" --count 1 --instance-type t2.micro --key-name mur_lopez_m3_act2_wordpress --security-group-ids "$AWS_DEFAULT_SG" "$AWS_WS_SG" "$AWS_SSH_SG" 
```

Ahora accedemos a la instancia EC2 que acabamos de crear por SSH:

```bash
ssh -lubuntu -i /RUTA_A_CLAVE_PUBLICA.pub IP_INSTANCIA

```

Una vez dentro de la máquina podemos comenzar la instalación de Wordpress:

```bash
sudo apt-get update
# Instalación del servidor Web
sudo apt-get install nginx -y

# Instalación del intérprete de PHP
sudo apt-get install php7.2-fpm php7.2-mysql -y
# Modificación de la configuración para que arregle paths érroneos
sudo sed -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" -i /etc/php/7.2/fpm/php.ini

# Reiniciamos el servicio de PHP
sudo systemctl restart php7.2-fpm

# Obtenemos una copia del software y lo descomprimimos en /var/www/hmtl
wget -O - https://www.wordpress.org/latest.tar.gz | sudo tar xzf - -C /var/www/html

# Nos movemos al directorio objetivo
cd /var/www/html/wordpress

#En el lugar de RDS_ENDPOINT debemos colocar la dirección para acceder a la base de datos
sed -e "s/localhost/"RDS_ENDPOINT"/" -e "s/database_name_here/wordpress/" -e "s/username_here/wordpress/" -e "s/password_here/W0rdpress\!/" wp-config-sample.php | sudo tee wp-config.php
```
Una vez realizados estos pasos se puede acceder al blog de wordpress para realizar la instalación y configurarlo en la URL o IP de la instancia EC2 en la ruta /wp-admin/install.php a través de un navegador.

## Instalación mediante Terraform

Necesitaremos descargar el binario de terraform para nuestra distribución. Una vez instalado seguir los siguientes pasos desde el directorio actual.

Antes de nada cambiar en la línea 8 la localización donde se encuentra el archivo con la clave pública.

Inicializar el proyecto terraform con sus plugins, etc.
```bash
terraform init
```

Formatear y validar la infraestructura definida
```bash
terraform fmt
terraform validate
```

Planificar los cambios que van a ser necesarios realizar en la infraestructura actual.
```bash
terraform plan
```

Si estamos de acuerdo con la planificación, se puede proceder a su aplicación
```bash
terraform apply
```

Tras aplicar la configuración visitar en enlace que aparece en la salida del comando para finalizar la instalación.

En caso de quere eliminar esta infraestructura
```bash
terraform destroy
```