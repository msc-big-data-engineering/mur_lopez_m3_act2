#!/bin/bash

sudo apt-get update
sudo apt-get install nginx -y

sudo apt-get install php7.2-fpm php7.2-mysql -y
sudo sed -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" -i /etc/php/7.2/fpm/php.ini
# EXT_LINE_NUMBER=`grep -n "; Module Settings ;" /etc/php/7.2/fpm/php.ini | cut -f1 -d:`

# sed -i "$(($EXT_LINE_NUMBER-2))iextension=mcrypt.so" /etc/php/7.2/fpm/php.ini

sudo systemctl restart php7.2-fpm

wget -O - https://www.wordpress.org/latest.tar.gz | sudo tar xzf - -C /var/www/html

cd /var/www/html/wordpress

# Build our wp-config.php file
sed -e "s/localhost/"${db_host}"/" -e "s/database_name_here/"${db_name}"/" -e "s/username_here/"${db_user}"/" -e "s/password_here/"${db_pass}"/" wp-config-sample.php | tee wp-config.php

# Grab our Salt Keys
SALT=$(curl -L https://api.wordpress.org/secret-key/1.1/salt/)
STRING='MscBigDataEngineer'
printf '%s\n' "g/$STRING/d" a "$SALT" . w | ed -s wp-config.php


sudo chown -R www-data:www-data /var/www/html/wordpress

cat << EOF | sudo tee /etc/nginx/sites-available/wordpress
server {    
listen   80;

	root /var/www/html/wordpress;
	index index.php index.html index.htm;

	server_name _;

	location / {
		try_files \$uri \$uri/ /index.php?\$args;
	}
	location = /favicon.ico {
       		log_not_found off;
	        access_log off;
    	}

	# pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
	location ~ \.php$ {
		fastcgi_intercept_errors on;
		#fastcgi_pass 127.0.0.1:9000;
		# With php5-fpm:
		fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
		fastcgi_index index.php;
	    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
		include fastcgi_params;
	}

	location ~ /\. {
        	deny all;
    	}
    
	location ~* /(?:uploads|files)/.*\.php$ {
       		deny all;
    	}
}
EOF

sudo ln -s /etc/nginx/sites-available/wordpress /etc/nginx/sites-enabled/wordpress 
sudo rm /etc/nginx/sites-enabled/default

sudo systemctl restart nginx
sudo systemctl enable nginx