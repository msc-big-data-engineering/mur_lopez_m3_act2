provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_key_pair" "mur_lopez_m3_act2_wordpress" {
  key_name   = "mur_lopez"
  public_key = file("~/.ssh/terraform.pub")
}

data "template_file" "init" {
  template = "${file("./install-wp.sh.tpl")}"

  vars = {
    db_host = "${aws_db_instance.mur_lopez_m3_act2_wordpress.address}"
    db_name = "${aws_db_instance.mur_lopez_m3_act2_wordpress.name}"
    db_user = "${aws_db_instance.mur_lopez_m3_act2_wordpress.username}"
    db_pass = "${aws_db_instance.mur_lopez_m3_act2_wordpress.password}"
  }
}

data "aws_vpc" "default" {
  default = true
}

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group" "webserver" {
  name = "webserver"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ssh" {
  name = "ssh"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "mur_lopez_m3_act2_wordpress" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mariadb"
  engine_version       = "10.3"
  instance_class       = "db.t2.micro"
  name                 = "wordpress"
  username             = "wordpress"
  password             = "W0rdpress!"
  parameter_group_name = "default.mariadb10.3"
  skip_final_snapshot = true
  vpc_security_group_ids = [data.aws_security_group.default.id]
}

resource "aws_instance" "mur_lopez_m3_act2_wordpress" {
  key_name               = aws_key_pair.mur_lopez_m3_act2_wordpress.key_name
  ami                    = "ami-046842448f9e74e7d"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [
    aws_security_group.webserver.id, 
    aws_security_group.ssh.id,
    data.aws_security_group.default.id
  ]

  tags = {
    Name = "mbde-wordpress"
  }

  user_data = data.template_file.init.rendered

}

output "public_ip" {
  value       = aws_instance.mur_lopez_m3_act2_wordpress.public_ip
  description = "The public IP of the web server"
}

output "public_db_ip" {
  value       = aws_db_instance.mur_lopez_m3_act2_wordpress.address
  description = "The public IP of the database"
}

output "finish_wp_install" {
  value = "http://${aws_instance.mur_lopez_m3_act2_wordpress.public_ip}/wp-admin/install.php"
  description="Visit for finish the installation"
}